import os.path
import argparse

import mido

notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

def MDNoteName(n):
	return "{}{}".format(notes[n % 12], n // 12)

# Generate list of frequencies indexed by MIDI note number
freqs = []
a = 440  # A is 440Hz
for i in range(0, 127):
	freqs.append(2**((i-69) / 12) * 440)

def MDNoteFreq(n):
	return int(round(freqs[n]))

mid = 0  # Temporary declaration for global

def openFile(inputFile):
	global mid
	mid = mido.MidiFile(inputFile)

def sTo10ms(n):
	return int(n * 100)


def convert(inputFile, outputFile, trackNum=-1):
	openFile(inputFile)
	f = open(outputFile, 'w')

	f.write("float songSpeed = 1.0;  // the speed multiplier of the song's playback\n\n")
	f.write('task pSong{}'.format(os.path.splitext(os.path.basename(outputFile))[0].title() + '() {\n'))
	# Two separate strings due to an error involving mismatched braces

	if trackNum == -1:  # Guess the best track to use
		mostMsgs = 0
		for i, track in enumerate(mid.tracks):
			msgCount = len(list(filter(lambda x: x.type == 'note_on', track)))
			if msgCount > mostMsgs:
				mostMsgs = msgCount
				trackNum = i + 1

	track = mid.tracks[trackNum - 1]
	for i in range(len(track) - 1):  # - 1 Because a note_on cannot occur as the last message in a track. Also prevents OOB errors.
		msg = track[i]
		curTempo = 500000  # Default. 120 BPM.
		if msg.type == 'set_tempo':
			curTempo = msg.tempo
		# The time attribute of a message is the amount of time since the last message/filestart. Therefore, the time
		# that a note plays is not stored in the note_on or note_off message, but the next message(s) of the track. It
		# can be found as the sum of the times of the messages between the note_on and the note_off, exclusive on the
		# former and inclusive on the latter bound.
		elif msg.type == 'note_on':
			# Find sum of times to (and including) the corresponding note_off
			playTime = 0
			for j in range(i + 1, len(track)):
				playTime += mido.tick2second(track[j].time, mid.ticks_per_beat, curTempo)
				if track[j].type == 'note_off' and track[j].note == msg.note:
					break
			playTime = sTo10ms(playTime)

			f.write('	playTone({}, {} * songSpeed);\n'.format(MDNoteFreq(msg.note), playTime))
		elif msg.type == 'note_off':
			# Find next note_on, if any.
			# The message at track[i + 1] should probably be a note_on,
			# but this is not necessarily the case if the track has overlapping notes.
			onIndex = -1
			for j in range(i + 1, len(track)):
				if track[j].type == 'note_on':
					onIndex = j
					break

			if onIndex != -1:
				onMsg = track[onIndex]
				# Find sum of times to (and including) the next note_on
				stopTime = 0
				for j in range(i + 1, onIndex + 1):
					stopTime += mido.tick2second(track[j].time, mid.ticks_per_beat, curTempo)
				stopTime = sTo10ms(stopTime)

				if stopTime != 0:  # Prevent 0ms waits
					f.write('	wait10Msec({} * songSpeed);\n'.format(stopTime))
			# else end of tracks

	f.write('}\n')
	f.close()

# CLI using argparse
parser = argparse.ArgumentParser()
parser.add_argument('inputFile', help="the MIDI file to read")
parser.add_argument('outputFile', help="the filename to write to")
parser.add_argument('trackNum', nargs='?', default='-1', type=int, help="the track to read; will be guessed if omitted")
args = parser.parse_args()
convert(args.inputFile, args.outputFile, args.trackNum)
