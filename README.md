# MIDI-to-RobotC
A Python 3 script to convert a MIDI file to RobotC code. Intended for use with Vex EDR.

## Requirements
```
$ pip install mido
```

## Usage
```
$ python m2rc.py -h
usage: m2rc.py [-h] inputFile outputFile [trackNum]

positional arguments:
  inputFile   the MIDI file to read
  outputFile  the filename to write to
  trackNum    the track to read; will be guessed if omitted

optional arguments:
  -h, --help  show this help message and exit
```

## License
This project is licensed under the GNU GPLv3 license. See [LICENSE](LICENSE).
